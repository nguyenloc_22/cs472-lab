package edu.mum.cs.cs472.lab11.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(
	name = "ContactFormController", 
	urlPatterns = {"/contact-form"}, 
	description = "ContactFormController")
public class ContactFormController extends HttpServlet {

    /**
	 * 
	 */
	private static final long serialVersionUID = -3434493683492454745L;

	protected void doPost(
		HttpServletRequest request, 
		HttpServletResponse response) 
		throws ServletException, IOException {
        
		doGet(request, response);
    }

    protected void doGet(
		HttpServletRequest request, 
		HttpServletResponse response) 
		throws ServletException, IOException {
        
    	request
    	.getServletContext()
    	.getRequestDispatcher("/contact-form.jsp")
    	.forward(request, response);
    }

}
