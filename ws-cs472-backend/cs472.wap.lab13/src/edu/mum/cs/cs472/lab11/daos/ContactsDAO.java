package edu.mum.cs.cs472.lab11.daos;

import edu.mum.cs.cs472.lab11.model.ContactFormData;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ContactsDAO {
	
	private final String DbName = "cs472_contacts";
	private final String TableName = "contacts";

	// @Resource(name = "jdbc/cs472_contacts")
    private DataSource dataSource;

    public ContactsDAO() {
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:comp/env");
            String lookupName = String.format("jdbc/%s", DbName);
            this.dataSource = (DataSource) envContext.lookup(lookupName);
        } catch (NamingException e) {
            System.err.println(e);
        }
    }

    public List<ContactFormData> getAllContactFormData() {
        List<ContactFormData> list = new ArrayList<>();
        
        String queryTemplate = 
    		"SELECT contacts_id, customer_name, gender, category, message FROM `%s`.%s order by customer_name";
        String query = String.format(queryTemplate, DbName, TableName);
        
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement pstmt = connection.prepareStatement(query);
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()) {
                int contactsId = rs.getInt("contacts_id");
                String name = rs.getString("customer_name");
                String gender = rs.getString("gender");
                String category = rs.getString("category");
                String message = rs.getString("message");
                ContactFormData data = new ContactFormData(
                		contactsId, name, gender, category, message);
                list.add(data);
            }
        } catch (SQLException e) {
            System.err.println(e);
        }
        return list;
    }
    
    public List<ContactFormData> searchAllContactFormData(String material) {
        List<ContactFormData> list = new ArrayList<>();
        
        String s = "%"+ material +"%";
        String queryTemplate = "select * from `%s`.`%s` "
        		+ "where customer_name like '%s' "
        		+ "or message like '%s' "
        		+ "or gender like '%s' "
        		+ "or category like '%s'";
        String query = String.format(queryTemplate, DbName, TableName,
        		s, s, s, s);
        
        System.out.println(query);
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pstmt = connection.prepareStatement(query);
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()) {
                int contactsId = rs.getInt("contacts_id");
                String name = rs.getString("customer_name");
                String gender = rs.getString("gender");
                String category = rs.getString("category");
                String message = rs.getString("message");
                ContactFormData data = new ContactFormData(
                		contactsId, name, gender, category, message);
                list.add(data);
            }
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
			if(connection != null) {
				try {
					connection.close();
				} catch(Exception ex) {}
			}
		}
        return list;
    }

    public ContactFormData saveContactFormData(ContactFormData contactFormData) {
    	
    	String queryTemplate = 
        		"insert into `%s`.%s(customer_name, gender, category, message) values (?, ?, ?, ?)";
        String query = String.format(queryTemplate, DbName, TableName);
            
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement pstmt = connection.prepareStatement(query);
            pstmt.setString(1, contactFormData.getName());
            pstmt.setString(2, contactFormData.getGender());
            pstmt.setString(3, contactFormData.getCategory());
            pstmt.setString(4, contactFormData.getMessage());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.err.println(e);
        }
        return contactFormData;
    }
}
