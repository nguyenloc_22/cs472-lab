<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="description" content="CS472-WAP" />
  <meta name="keywords" content="HTML, CSS" />
  
  <title>CS472-WAP-Lab 10 Solution</title>
  <link 
    href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/cosmo/bootstrap.min.css" 
    rel="stylesheet" 
    integrity="sha384-uhut8PejFZO8994oEgm/ZfAv0mW1/b83nczZzSwElbeILxwkN491YQXsCFTE6+nx" 
    crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/page-auth.css">
  <title>CS472 ::: WAP - Contact Us</title>
</head>
<body>

<div class="container">
  <div class="row">
	
    <div class="col-md-5 mx-auto">

      <!-- Sign up form -->
      <div id="second">
        <div class="myform form ">
          <div class="logo mb-3">
            <div class="col-md-12 text-center">
              <h1 >Signup</h1>
            </div>
          </div>
        
          <form action="#" method="post" name="registration">
          
            <!-- First Name -->
            <div class="form-group">
              <label for="exampleInputEmail1">First Name</label>
              <input type="text" name="firstname" class="form-control" id="firstname" aria-describedby="emailHelp" placeholder="Enter Firstname">
            </div>
            
            <!-- Last Name -->
            <div class="form-group">
              <label for="exampleInputEmail1">Last Name</label>
              <input type="text" name="lastname" class="form-control" id="lastname" aria-describedby="emailHelp" placeholder="Enter Lastname">
            </div>
            
            <!-- Email address -->
            <div class="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input type="email" name="email"  class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email">
            </div>
            
            <!-- Password -->
            <div class="form-group">
              <label for="exampleInputEmail1">Password</label>
              <input type="password" name="password" id="password"  class="form-control" aria-describedby="emailHelp" placeholder="Enter Password">
            </div>
            
            <!-- Button Submit -->
            <div class="col-md-12 text-center mb-3">
              <button type="submit" class=" btn btn-block mybtn btn-primary tx-tfm">Get Started For Free</button>
            </div>
            
            <!-- Already have an account ? -->
            <div class="col-md-12 ">
              <div class="form-group">
                 <p class="text-center"><a href="#" id="signin">Already have an account?</a></p>
              </div>
            </div>
          
          </form>
        
        </div>
      </div>
    </div>
  </div>
</div> 

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
</body>
</html>