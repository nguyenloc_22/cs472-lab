package mum.cs.cs472.todo.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Simple console logger implementation
 * @author locnv
 *
 */
public class Logger {
	
	private String name;
	private static Map<String, Logger> mLogger;
	
	static {
		mLogger = new HashMap<String, Logger>();
	}
	
	private Logger(String name) {
		this.name = name;
	}
	
	public static Logger getLogger(String name) {
		Logger logger = mLogger.get(name);
		if(logger == null) {
			logger = new Logger(name);
			mLogger.put(name, logger);
		}
		
		return logger;
	}
	
	public void debug(String msg) {
		System.out.println(String.format("[debug][%s] %s", name, msg));
	}
	
	public void info(String msg) {
		System.out.println(String.format("[info][%s] %s", name, msg));
	}
	
	public void warn(String msg) {
		System.out.println(String.format("[warn][%s] %s", name, msg));
	}
	
	public void warn(String msg, Throwable t) {
		System.out.println(String.format("[warn][%s] %s", name, msg));
		t.printStackTrace();
	}
	
	public void error(String msg) {
		System.out.println(String.format("[error][%s] %s", name, msg));
	}
	
	public void error(String msg, Throwable t) {
		System.out.println(String.format("[info][%s] %s", name, msg));
		t.printStackTrace();
	}
}
