package mum.cs.cs472.todo.service;

import mum.cs.cs472.todo.model.User;

public interface IUserService {
	boolean login(User user);
}
