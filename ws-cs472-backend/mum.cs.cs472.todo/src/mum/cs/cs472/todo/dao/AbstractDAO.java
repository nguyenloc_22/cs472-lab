package mum.cs.cs472.todo.dao;

import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import mum.cs.cs472.todo.model.BaseModel;
import mum.cs.cs472.todo.util.Logger;

public abstract class AbstractDAO<T extends BaseModel<ID>, ID> {
	
	private final String DbCtxName = "jdbc/cs472-201911-lesson15-contacts-db";
	private final String DbName = "cs472-contact-form";
	
	private final static String FindAllQueryTemplate = "SELECT * FROM `%s`.%s";
	
	protected Logger logger;
	
	// @Resource(name = "jdbc/cs472-201911-lesson15-contacts-db")
	private DataSource dataSource;
	
	public AbstractDAO() {
		
		logger = Logger.getLogger(this.getClass().getCanonicalName());
		
		try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:comp/env");
            this.dataSource = (DataSource) envContext.lookup(DbCtxName);
        } catch (NamingException e) {
            logger.error("Failed to initialize dataSource for " + DbCtxName, e);
        }
	}
	
	protected T getInstanceOfT() {
		ParameterizedType superClass = (ParameterizedType) getClass().getGenericSuperclass();
        @SuppressWarnings("unchecked")
		Class<T> type = (Class<T>) superClass.getActualTypeArguments()[0];
        try {
            return type.newInstance();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
	}
	
	public T findById(ID id) throws SQLException {
		
		String query = String.format(FindAllQueryTemplate, DbCtxName, DbName);
		try {
			Connection connection = dataSource.getConnection();
            PreparedStatement pstmt = connection.prepareStatement(query);
            ResultSet rs = pstmt.executeQuery();
            
            T t = getInstanceOfT();
            String[] columnNames = t.getColumnNames();
            Object[] data = new Object[columnNames.length];
            
            if(!rs.next()) {
            	return null;
            }
            
        	for(int i = 0; i < columnNames.length; i++) {
        		String key = columnNames[i];
        		Object d = rs.getObject(key);
        		data[i] = d;
        	}
        	
        	t.loadData(data);
        	
        	return t;

//	            int contactsId = rs.getInt("contacts_id");
//	            String name = rs.getString("customer_name");
//	            String gender = rs.getString("gender");
//	            String category = rs.getString("category");
//	            String message = rs.getString("message");
//	            ContactFormData data = new ContactFormData(contactsId, name, gender, category, message);
		} catch(SQLException e) {
			logger.error("An error occured while executing findById.", e);
			throw e;
		}
		
	}
	
	public List<T> findAll() {
		// TODO implement
		return null;
	}
	
	public T insert(T model) {
		return null;
	}
	
}
