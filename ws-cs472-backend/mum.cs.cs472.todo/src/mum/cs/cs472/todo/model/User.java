package mum.cs.cs472.todo.model;

import java.util.Date;

import mum.cs.cs472.todo.type.UserRole;

public class User extends BaseModel<Long> {
	
	private final String TableName = "user";
	private final String[] ColumnNames = { 
			"id", "user_name", "password", "first_name", "last_name", "email", "dob" };
	
	private long id;
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	private String email;
	private UserRole role;
	private Date dob;
	
	public User() {
		
	}
	
	public User(
			String username, 
			String password, 
			String firstName, 
			String lastName, 
			String email, 
			Date dob) {
		super();
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.dob = dob;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public UserRole getRole() {
		return role;
	}
	
	public void setRole(UserRole role) {
		this.role = role;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}
	
	@Override
	public String toString() {
		return String.format("User {"
				+ "userName: %s,"
				+ "firstName: %s,"
				+ "lastName: %s,"
				+ "..."
				+ "}", 
				username, firstName, lastName);
	}
	
	@Override
	public String getTableName() {
		return TableName;
	}
	
	@Override
	public String[] getColumnNames() {
		return ColumnNames;
	}
	
	@Override
	public void loadData(Object[] data) {
		id = (long) data[0];
		username = data[1].toString();
		password = data[2].toString();
		firstName = data[3].toString();
		lastName = data[4].toString();
		email = data[5].toString();
		
		String roleString = data[6].toString();
		role = UserRole.valueOf(roleString);
		dob = null; // TODO Convert java date
	}
	
}
