package mum.cs.cs472.todo.model;

import java.util.Date;

import mum.cs.cs472.todo.type.ProjectStatus;

public class Project extends BaseModel<Long> {
	
	private final String TableName = "project";
	private final String[] ColumnNames = { 
			"id", "name", "description", "created_date", "status" };
	
	private long id;
	private String name;
	private String description;
	private ProjectStatus status;
	private Date createdDate;
	
	public Project() { }

	public Project(long id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.status = ProjectStatus.New;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ProjectStatus getStatus() {
		return status;
	}

	public void setStatus(ProjectStatus status) {
		this.status = status;
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}
	
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Override
	public String toString() {
		
		return String.format("Project: {"
				+ "id: %s,"
				+ "name: %s,"
				+ "description: %s}", 
				id, name, description);
	}
	
	@Override
	public String getTableName() {
		return TableName;
	}
	
	@Override
	public String[] getColumnNames() {
		return ColumnNames;
	}
	
	@Override
	public void loadData(Object[] data) {
		// TODO Implement --> parsing object to property data
	}

}
