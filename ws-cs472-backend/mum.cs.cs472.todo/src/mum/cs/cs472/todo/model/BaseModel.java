package mum.cs.cs472.todo.model;

public abstract class BaseModel<ID> {
	protected ID id;
	
	public String getIdColumnName() {
		return "id";
	}
	
	public abstract String getTableName();
	public abstract String[] getColumnNames();
	public abstract void loadData(Object[] data);
}
