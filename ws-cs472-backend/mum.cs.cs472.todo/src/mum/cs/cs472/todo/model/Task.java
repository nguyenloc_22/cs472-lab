package mum.cs.cs472.todo.model;

import java.util.Date;

public class Task extends BaseModel<Long> {
	
	private final String TableName = "task";
	private final String[] ColumnNames = { 
			"id", "title", "description", "project", "created_date", "due_date" };
	
	private long id;
	private String title;
	private String description;
	private Project project;
	private Date createdDate;
	private Date dueDate;
	
	public Task() { }

	public Task(String title, String description) {
		super();
		this.title = title;
		this.description = description;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	@Override
	public String toString() {
		return String.format("Task: {"
				+ "id: %s,"
				+ "title: %s,"
				+ "description: %s"
				+ "}", id, title, description);
	}
	
	@Override
	public String getTableName() {
		return TableName;
	}
	
	@Override
	public String[] getColumnNames() {
		return ColumnNames;
	}
	
	@Override
	public void loadData(Object[] data) {
		// TODO Auto-generated method stub
		
	}
}
