package mum.cs.cs472.todo.type;

public enum ProjectStatus {
	New,
	InProcess,
	Finished,
	Closed,
	Canceled
}
