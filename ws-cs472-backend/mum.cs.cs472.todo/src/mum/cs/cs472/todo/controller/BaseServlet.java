package mum.cs.cs472.todo.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mum.cs.cs472.todo.util.Logger;

public abstract class BaseServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4170304382675640356L;
	
	protected boolean traceRequest = true;
	
	protected Logger logger;
	
	public BaseServlet() {
		super();
		
		logger = Logger.getLogger(this.getClass().getCanonicalName());
		
		logger.debug("Initialization of controller ::: " + this.getClass().getName());
	}
	
	@Override
	protected void doGet(
		HttpServletRequest req, 
		HttpServletResponse resp) 
			throws ServletException, IOException {
		// super.doGet(req, resp);
		
		traceRequest(req);
	}
	
	@Override
	protected void doPost(
		HttpServletRequest req, 
		HttpServletResponse resp) 
			throws ServletException, IOException {
		super.doPost(req, resp);
	}
	
	private void traceRequest(HttpServletRequest req) {
		if(!traceRequest) {
			return;
		}
		
		String ctxPath = req.getContextPath();
		String localAddr = req.getLocalAddr();
		String pathInfo = req.getPathInfo();
		String protocol = req.getProtocol();
		String reqUri = req.getRequestURI();
		
		StringBuilder sb = new StringBuilder();
		sb.append("\nContextPath: ").append(ctxPath);
		sb.append("\nLocalAddr: ").append(localAddr);
		sb.append("\nPathInfo: ").append(pathInfo);
		sb.append("\nProtocol: ").append(protocol);
		sb.append("\nReqUrl: ").append(reqUri);
		
		logger.debug(sb.toString());
	}

}
