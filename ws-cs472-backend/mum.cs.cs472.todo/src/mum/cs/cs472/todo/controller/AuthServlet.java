package mum.cs.cs472.todo.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AuthServlet
 */
@WebServlet("/auth")
public class AuthServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
	
	private final static String SignInPath = "/signin";
	private final static String SignUpPath = "/signup";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AuthServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(
		HttpServletRequest request, 
		HttpServletResponse response) 
			throws ServletException, IOException {
		
		super.doGet(request, response);

		// response.getWriter().append("Served at: ").append(request.getContextPath());
		String reqUri = request.getRequestURI();
		String targetJspPath = "/jsp/page/signin.jsp";
		if(reqUri.endsWith(SignUpPath)) {
			targetJspPath = "/jsp/page/signup.jsp";
		}
		
		request
        .getRequestDispatcher(targetJspPath)
        .forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(
		HttpServletRequest request, 
		HttpServletResponse response) 
			throws ServletException, IOException {

		logger.debug("doPost --> ");
		doGet(request, response);
	}

}
