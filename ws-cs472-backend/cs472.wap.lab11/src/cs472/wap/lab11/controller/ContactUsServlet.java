package cs472.wap.lab11.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cs472.wap.lab11.util.Logger;

/**
 * Servlet implementation class ContactUs
 */
@WebServlet("/contact-us")
public class ContactUsServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private Logger logger;
	
	private static int hitCounter; 
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ContactUsServlet() {
        super();
        
        logger = Logger.getLogger("contact-us");
        logger.debug("Contact us servlet constructor.");
    }
    
    @Override
    public void init() throws ServletException {
    	super.init();
    	
    	hitCounter = 0;
    	logger.debug("ContactUs Controller --> hitCounter -> " + hitCounter);
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(
		HttpServletRequest request, 
		HttpServletResponse response) throws ServletException, IOException {
		
		logger.debug("doGet -> ");
		hitCounter ++;

		request.setAttribute("pageTitle", "Contact Us");
		request.setAttribute("hitCounter", hitCounter);
        request
        .getRequestDispatcher("/jsp/page/contact-form.jsp")
        .forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(
		HttpServletRequest request, 
		HttpServletResponse response) throws ServletException, IOException {
		logger.debug("doPost -> ");
		doGet(request, response);
	}

}
