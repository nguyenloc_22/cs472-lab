package cs472.wap.lab11.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cs472.wap.lab11.util.Logger;

/**
 * Servlet implementation class Welcome
 */
@WebServlet("/thank-you")
public class ThankyouServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private Logger logger;
	private static int hitCounter = 0;

	private static Map<String, String> ContactFormKeyMap;
	
	static {
		ContactFormKeyMap = new HashMap<String, String>();
		
		ContactFormKeyMap.put(ContactFormKey.TextCategoryKey, "Category");
		ContactFormKeyMap.put(ContactFormKey.TextGenderKey, "Gender");
		ContactFormKeyMap.put(ContactFormKey.TextMessageKey, "Message");
		ContactFormKeyMap.put(ContactFormKey.TextNameKey, "Name");
	}
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ThankyouServlet() {
        super();
        
        logger = Logger.getLogger("welcome");
        logger.debug("Thank-you servlet constructor.");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(
		HttpServletRequest request, 
		HttpServletResponse response) throws ServletException, IOException {
		
		logger.debug("[thank-you] doGet -> ");
		hitCounter ++;
		response.setContentType("text/html");
		
		request.setAttribute("pageTitle", "Thank you!");
		request.setAttribute("hitCounter", hitCounter);
		request.setAttribute("contact", request.getParameterMap());
		
        request
        .getRequestDispatcher("/jsp/page/thankyou.jsp")
        .forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(
		HttpServletRequest request, 
		HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}
}
