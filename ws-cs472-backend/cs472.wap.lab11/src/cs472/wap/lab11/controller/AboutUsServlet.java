package cs472.wap.lab11.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cs472.wap.lab11.util.Logger;

/**
 * Servlet implementation class ContactUs
 */
@WebServlet("/about-us")
public class AboutUsServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private Logger logger;
	
	private static int hitCounter; 
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AboutUsServlet() {
        super();
        
        logger = Logger.getLogger("about-us");
        logger.debug("About us servlet constructor.");
    }
    
    @Override
    public void init() throws ServletException {
    	super.init();
    	
    	hitCounter = 0;
    	logger.debug("AboutUs Controller --> hitCounter -> " + hitCounter);
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(
		HttpServletRequest request, 
		HttpServletResponse response) throws ServletException, IOException {
		
		logger.debug("doGet -> ");
		hitCounter ++;

		request.setAttribute("pageTitle", "About Us");
		request.setAttribute("hit-counter", hitCounter);
		request.setAttribute("hitCounter", hitCounter);
		// request.setAttribute("data", new Object());
        request
        .getRequestDispatcher("/jsp/page/about-us.jsp")
        .forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(
		HttpServletRequest request, 
		HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("needValidate", true);
		logger.debug("doPost -> ");
		
		request.setAttribute("contact", request.getParameterMap());
		
		doGet(request, response);
	}

}
