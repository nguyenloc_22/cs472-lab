package mum.cs.cs472.wap.student.service.impl;

import java.util.ArrayList;
import java.util.List;

import mum.cs.cs472.wap.student.model.Student;
import mum.cs.cs472.wap.student.service.IStudentService;

public class StudentService implements IStudentService {
	
	private List<Student> students;
	
	public StudentService() {
		students = new ArrayList<Student>();
		
		Student john = new Student("cs-aug-0001", "Jhon Smith");
		Student marry = new Student("cs-aug-0002", "Marry Kan");
		
		students.add(john);
		students.add(marry);
	}

	@Override
	public List<Student> getAllStudent() {
		return students;
	}
	
}
