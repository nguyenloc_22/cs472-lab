package mum.cs.cs472.wap.student.service.impl;

import java.util.HashMap;
import java.util.Map;

import mum.cs.cs472.wap.student.service.IStudentService;

public class ServiceFactory {
	
	private static ServiceFactory THIS = null;
	
	private Map<String, Object> servicesMap;
	
	private ServiceFactory() {
		servicesMap = new HashMap<String, Object>();
	}
	
	public static ServiceFactory getInstance() {
		if(THIS == null) {
			THIS = new ServiceFactory();
		}
		
		return THIS;
	}
	
	public Object getService(Class<?> clazz) {
		String className = clazz.getName();
		Object service = servicesMap.get(className);
		if(service == null) {
			switch (className) {
			case "IStudentService":
				service = new StudentService();
				break;

			default:
				break;
			}
			
			if(service != null) {
				servicesMap.put(className, service);
			}
		}
		
		return service;
	}
}
