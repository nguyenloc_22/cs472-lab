package mum.cs.cs472.wap.student.model;

public class Student {
	private long id;
	private String fullName;
	private String studentId;
	
	public Student() {
		
	}
	
	public Student(String studentId, String fullName) {
		this.fullName = fullName;
		this.studentId = studentId;
	}
	
	public long getId() {
		return id;
	}
	
	public String getFullName() {
		return fullName;
	}
	
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public String getStudentId() {
		return studentId;
	}
	
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	
}
