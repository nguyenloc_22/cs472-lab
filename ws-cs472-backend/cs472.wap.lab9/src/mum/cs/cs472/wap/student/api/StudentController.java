package mum.cs.cs472.wap.student.api;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mum.cs.cs472.wap.student.service.IStudentService;
import mum.cs.cs472.wap.student.service.impl.ServiceFactory;
import mum.cs.cs472.wap.student.util.Logger;

/**
 * Servlet implementation class StudentController
 */
@WebServlet("/student")
public class StudentController extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private Logger logger;
	
	private IStudentService studentService;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentController() {
        super();
    
        studentService = (IStudentService) ServiceFactory.getInstance().getService(IStudentService.class.getClass());
        
        logger = Logger.getLogger("student-ctrl");
        logger.info("Student Controller - constructor");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(
		HttpServletRequest request, 
		HttpServletResponse response) throws ServletException, IOException {
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(
		HttpServletRequest request, 
		HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		
		logger.debug("doPost ...");
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		// doGet(request, response);
		String studentJsonString = "{ name: \"John\", studentId: \"mum-cs-0001\"}";
		out.print(studentJsonString);
		out.flush();
	}

}
