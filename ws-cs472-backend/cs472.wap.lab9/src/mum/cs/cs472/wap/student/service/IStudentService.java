package mum.cs.cs472.wap.student.service;

import java.util.List;

import mum.cs.cs472.wap.student.model.Student;

public interface IStudentService {
	public List<Student> getAllStudent();
}
