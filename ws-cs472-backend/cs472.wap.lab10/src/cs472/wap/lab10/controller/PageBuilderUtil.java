package cs472.wap.lab10.controller;

public class PageBuilderUtil {
	public static String buildHeader() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("<head>");
		sb.append("  <meta charset=\"utf-8\" />");
		sb.append("  <meta name=\"description\" content=\"CS472-WAP\" />");
		sb.append("  <meta name=\"keywords\" content=\"HTML, CSS\" />");
		sb.append("  <link rel=\"stylesheet\" type=\"text/css\" href=\"css/contact-us.css\">");
		sb.append("  <title>CS472: WAP ::: Lab 10</title>");
		sb.append("</head>");
		
		return sb.toString();
	}
	
	public static String buildPageHeader() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("<header>");
		sb.append("<a href=\"/lab10\">CS472 : WAP ::: Lab 10</a>");
		sb.append("</header>");
		
		return sb.toString();
	}
	
	public static String buildPageFooter(int hits) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("<footer>");
		sb.append("Copyright @610513 | Hit Counter: "+ hits +" | Total Hit Counter: " + HitCounterFilter.getHitCounter());
		sb.append("</footer>");
		
		return sb.toString();
	}
	
}
