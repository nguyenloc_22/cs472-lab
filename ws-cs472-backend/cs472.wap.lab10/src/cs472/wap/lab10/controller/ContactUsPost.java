package cs472.wap.lab10.controller;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cs472.wap.lab10.util.Logger;

/**
 * Servlet implementation class Welcome
 */
@WebServlet("/contact-us-post")
public class ContactUsPost extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
		
	private Logger logger;

       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ContactUsPost() {
        super();
        
        logger = Logger.getLogger("contact-us:::post");
        logger.debug("Contact-us Post servlet constructor.");
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(
		HttpServletRequest request, 
		HttpServletResponse response) throws ServletException, IOException {
		
		if(!validateContactUsForm(request)) {
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("/contact-us");
			reqDispatcher.forward(request, response);
		} else {
			String thankyouPath = getThankyouPathWithData(request);
			response.sendRedirect(thankyouPath);
		}
	}
	
	private String getThankyouPathWithData(HttpServletRequest req) {
		StringBuilder sb = new StringBuilder("/lab10/thank-you?");
		
		Enumeration<String> keys = req.getParameterNames();
		boolean isFirst = true;
		while(keys.hasMoreElements()) {
			
			if(!isFirst) {
				sb.append("&");
			} else {
				isFirst = false;
			}
			
			String k = keys.nextElement();
			String d = String.format("%s=%s", k, req.getParameter(k));
			sb.append(d);
		}
		
		return sb.toString();
	}
	
	private boolean validateContactUsForm(HttpServletRequest req) {
		boolean isValid = true;

		if(!isValid(req, ContactFormKey.TextGenderKey)) {
			isValid = false;
			req.setAttribute(String.format("%s-%s", ContactFormKey.TextGenderKey, "Error"), "This field cannot be empty.");
		}
		
		if(!isValid(req, ContactFormKey.TextNameKey)) {
			isValid = false;
			req.setAttribute(String.format("%s-%s", ContactFormKey.TextNameKey, "Error"), "This field cannot be empty.");
		}
		
		if(!isValid(req, ContactFormKey.TextCategoryKey)) {
			isValid = false;
			req.setAttribute(String.format("%s-%s", ContactFormKey.TextCategoryKey, "Error"), "This field cannot be empty.");
		}
		if(!isValid(req, ContactFormKey.TextMessageKey)) {
			isValid = false;
			req.setAttribute(String.format("%s-%s", ContactFormKey.TextMessageKey, "Error"), "This field cannot be empty.");
		}
		
		return isValid;
	}

	private boolean isValid(HttpServletRequest req, String paramKey) {
		
		Object obj = req.getParameter(paramKey);
		if(obj == null || obj.toString().isEmpty()) {
			return false;
		}
		
		return true;
	}

}
