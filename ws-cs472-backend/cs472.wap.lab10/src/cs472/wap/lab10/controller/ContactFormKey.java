package cs472.wap.lab10.controller;

public interface ContactFormKey {
	public static final String TextNameKey = "txtName";
	public static final String TextGenderKey = "rGender";
	public static final String TextCategoryKey = "selCategory";
	public static final String TextMessageKey = "txtMessage";
}
