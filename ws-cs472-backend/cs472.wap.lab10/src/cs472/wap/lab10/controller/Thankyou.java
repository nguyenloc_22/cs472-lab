package cs472.wap.lab10.controller;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cs472.wap.lab10.util.Logger;

/**
 * Servlet implementation class Welcome
 */
@WebServlet("/thank-you")
public class Thankyou extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private Logger logger;
	private static int hitCounter = 0;

	private static Map<String, String> ContactFormKeyMap;
	
	static {
		ContactFormKeyMap = new HashMap<String, String>();
		
		ContactFormKeyMap.put(ContactFormKey.TextCategoryKey, "Category");
		ContactFormKeyMap.put(ContactFormKey.TextGenderKey, "Gender");
		ContactFormKeyMap.put(ContactFormKey.TextMessageKey, "Message");
		ContactFormKeyMap.put(ContactFormKey.TextNameKey, "Name");
	}
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Thankyou() {
        super();
        
        logger = Logger.getLogger("welcome");
        logger.debug("Thank-you servlet constructor.");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(
		HttpServletRequest request, 
		HttpServletResponse response) throws ServletException, IOException {
		
		logger.debug("[thank-you] doGet -> ");
		hitCounter ++;
		response.setContentType("text/html");
		
		response.getWriter().write(buildThankyouPage(request));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(
		HttpServletRequest request, 
		HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private String buildThankyouPage(HttpServletRequest request) {
		
		logger.debug("Building thank-you page.");
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("<html lang=\"en\">");
		sb.append(PageBuilderUtil.buildHeader());
		sb.append("<body>");
		sb.append(PageBuilderUtil.buildPageHeader());
		
		sb.append("<div id=\"page\">");
		
		Enumeration<String> keys = request.getParameterNames();
		
		sb.append("<div class=\"frm-thankyou\">");
		sb.append("<h2>Thank you for submitting your request.</h2><br/>");
		sb.append("<h3>Your data: </h3>");
		sb.append("<ul>");
		while(keys.hasMoreElements()) {
			String k = keys.nextElement();
			String kDisplay = ContactFormKeyMap.get(k);
			if(kDisplay == null || kDisplay.isEmpty()) {
				kDisplay = k;
			}
			logger.debug("Form data -> " + k + "::: " + request.getParameter(k));
			sb.append("<li>"+ kDisplay + ": " + request.getParameter(k) +"</li>");
		}
		
		sb.append("</ul>");
		sb.append("</div>");
		
		sb.append("</div>");
		sb.append(PageBuilderUtil.buildPageFooter(hitCounter));
		sb.append("</body>");
		sb.append("</html>");
		
		return sb.toString();
	}
}
