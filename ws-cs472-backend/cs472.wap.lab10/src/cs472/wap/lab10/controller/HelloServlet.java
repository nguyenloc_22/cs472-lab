package cs472.wap.lab10.controller;

import java.io.IOException;
import java.time.LocalTime;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cs472.wap.lab10.util.Logger;

/**
 * Servlet implementation class HelloServlet
 */
@WebServlet(description = "CS472-WAP :::Hello Servlet", urlPatterns = { "/hello" })
public class HelloServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private final Logger logger;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HelloServlet() {
        super();
        
        logger = Logger.getLogger("hello-ctrl");
        
        logger.debug("Hello Servlet Constructor!!!");
    }
    
    @Override
    public void init() throws ServletException {
    	super.init();
    	
    	logger.debug("Hello Servlet Initialized!!!");
    }
    
    @Override
    public void destroy() {
    	super.destroy();
    	
    	logger.debug("Hello Servlet Destroyed!!!");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(
			HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		logger.debug("Handle doGet()");
		response.setContentType("text/html");
		
		StringBuffer sb = new StringBuffer();
		sb
		.append("<div class=\"page\">")
		.append("  <header><h2>CS472-WAP ::: Hello Servlet!!!</header></h2>")
		.append("  <div class=\"container\">")
		.append("    <h2>Hello Java Servlet 4.0</h2>")
		.append("    <small>Served at: "+ request.getContextPath() +"</small><br/>")
		.append("    <small>Server time: "+ LocalTime.now().toString() +"</small>")
		.append("  </div>")
		.append("</div>");
	
		response.getWriter().write(sb.toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.debug("Handle doPost()");
		doGet(request, response);
	}

}
