package cs472.wap.lab10.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cs472.wap.lab10.util.Logger;

/**
 * Servlet implementation class ContactUs
 */
@WebServlet("/contact-us")
public class ContactUs extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private Logger logger;
	
	private static int hitCounter; 
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ContactUs() {
        super();
        
        logger = Logger.getLogger("contact-us");
        logger.debug("Contact us servlet constructor.");
    }
    
    @Override
    public void init() throws ServletException {
    	super.init();
    	
    	hitCounter = 0;
    	logger.debug("ContactUs Controller --> hitCounter -> " + hitCounter);
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(
		HttpServletRequest request, 
		HttpServletResponse response) throws ServletException, IOException {
		
		logger.debug("doGet -> ");
		hitCounter ++;
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		PrintWriter pw = response.getWriter();
		
		pw.append(buildContactPage(request));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(
		HttpServletRequest request, 
		HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("needValidate", true);
		logger.debug("doPost -> ");
		
		doGet(request, response);
	}
	
	private String buildContactPage(HttpServletRequest request) {
		
		Object bObject = request.getAttribute("needValidate");
		boolean needValidate = false;
		if(bObject != null) {
			needValidate = (boolean)bObject;	
		}
		 
		logger.debug("Building contact page -> needValidation -> " + needValidate);
		
		Enumeration<String> keys = request.getParameterNames();
		while(keys.hasMoreElements()) {
			String k = keys.nextElement();
			logger.debug("Form data -> " + k + "::: " + request.getParameter(k));
		}
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("<!DOCTYPE html>");
		sb.append("<html lang=\"en\">");
		sb.append(PageBuilderUtil.buildHeader());
		
		sb.append("<body>");
		
		sb.append(PageBuilderUtil.buildPageHeader());
		
		sb.append("<div id=\"page\">");
		sb.append(buildContactForm(needValidate, request));
		sb.append("</div>");
		
		sb.append(PageBuilderUtil.buildPageFooter(hitCounter));
	
		sb.append("</body>");
		sb.append("</html>");
		
		return sb.toString();
	}
	
	private String buildContactForm(boolean needValidate, HttpServletRequest req) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("<form id=\"frm-contact\" method=\"POST\" action=\"/lab10/contact-us-post\" class=\"needs-validation\">");

		sb.append("<legend>Contact us!!!</legend>");
	    // <!-- Name -->
		sb.append("<div class=\"form-group\">");
		sb.append("  <label for=\"txtName\">Name</label>");
		sb.append("  <input type=\"text\" class=\"form-control\" id=\"txtName\" name=\"txtName\" required\"");
		sb.append("    aria-describedby=\"nameIdHelp\" placeholder=\"Your name\" value=\""+ getFormFieldValue(req, ContactFormKey.TextNameKey) +"\">");	
		sb.append("  <span class=\"error\">"+ getFormFieldError(req, "txtName-Error") +"</span>");		
		sb.append("</div>");

		// Gender
		String lastGender = getFormFieldValue(req, ContactFormKey.TextGenderKey);
		sb.append("<div class=\"form-group\">");
		sb.append("<label>Gender</label>");
		sb.append("<div>");
		sb.append("  <input id=\"gMale\" type=\"radio\""+ genderChecked("male", lastGender) + " name=\"rGender\" value=\"male\">");
		sb.append("  <label for=\"gMale\">&nbsp;Male</label>&nbsp;&nbsp;");
		sb.append("  <input id=\"gFeMale\" type=\"radio\""+ genderChecked("female", lastGender) +" name=\"rGender\" value=\"female\">");
		sb.append("  <label for=\"gFeMale\">&nbsp;Female</label>&nbsp;&nbsp;");
		sb.append("  <input id=\"gOther\" type=\"radio\""+ genderChecked("other", lastGender) +" name=\"rGender\" value=\"other\">");
		sb.append("  <label for=\"gOther\">&nbsp;Other</label>&nbsp;&nbsp;");
		sb.append("</div>");
		sb.append("<span class=\"error\">"+ getFormFieldError(req, "rGender-Error") +"</span>");		
		sb.append("</div>");

		// <!-- Category -->
		String lastCategory = getFormFieldValue(req, ContactFormKey.TextCategoryKey);
		sb.append("<div class=\"form-group\">");
		sb.append("  <label for=\"selCategory\">Category</label>");
		sb.append("  <select class=\"form-control\" id=\"selCategory\" name=\"selCategory\" required>");
		sb.append("    <option "+ categorySelected(lastCategory, "feedback") +" value=\"feedback\">Feedback</option>");
		sb.append("    <option "+ categorySelected(lastCategory, "inquiry") +" value=\"inquiry\">Inquiry</option>");
		sb.append("    <option "+ categorySelected(lastCategory, "complaint") +" value=\"complaint\">Complaint</option>");
		sb.append("  </select>");
		sb.append("<span class=\"error\">"+ getFormFieldError(req, "selCategory-Error") +"</span>");		
		sb.append("</div>");

		// <!-- Message -->
	    sb.append("<div class=\"form-group\">");
	    sb.append("  <label for=\"txtMessage\">Message</label>");
		sb.append("  <textarea type=\"text\" class=\"form-control\" id=\"txtMessage\" name=\"txtMessage\" placeholder=\"Enter your message\">");
		sb.append(getFormFieldValue(req, ContactFormKey.TextMessageKey));
		sb.append("</textarea>");
		sb.append("<span class=\"error\">"+ getFormFieldError(req, "txtMessage-Error") +"</span>");
		sb.append("</div>");

		sb.append("<button type=\"submit\" id=\"btnSubmit\" class=\"btn btn-primary\">Submit</button>");

		sb.append("</form>");
		
//		if(needValidate) {
//			sb.append("<p>"+ buildFormParams(req) +"</p>");
//		}

		return sb.toString();
	}
	
	private String genderChecked(String value, String option) {
		return value.equals(option) ? "checked" : "";
	}
	
	private String categorySelected(String value, String option) {
		return value.equals(option) ? "selected" : "";
	}
	
	private String getFormFieldValue(HttpServletRequest req, String key) {
		String v = req.getParameter(key);
		if(v == null) {
			return "";
		}
		
		return v;
	}
	
	private String getFormFieldError(HttpServletRequest req, String key) {
		Object obj = req.getAttribute(key);
		
		if(obj == null) {
			return "";
		}
		
		return obj.toString();
	}
	
//	private String buildFormParams(HttpServletRequest req) {
//		StringBuilder sb = new StringBuilder();
//		
//		sb.append("<b>Your submitted data:</b></br>");
//		Enumeration<String> keys = req.getParameterNames();
//		while(keys.hasMoreElements()) {
//			String k = keys.nextElement();
//			sb.append(k +" -->" + req.getParameter(k));
//			sb.append("</br>");
//		}
//		
//		return sb.toString();
//	}

}
