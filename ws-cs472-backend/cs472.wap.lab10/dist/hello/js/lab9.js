(function() {

  "use strict";

  // Constants
  const logger = console;
  const studentDbService = new StudentDbService();

  // Local variables
  let students = null;
  let ajax = new Ajax();

  // studentDbService.dropDb();

  // Schedule to start app's entry point.
  setTimeout(main, 1);

  // Entry point
  function main() {
    logger.info('CS472-WAP -> Lab 9');

    // Update current time
    readCurrentTime();

    // Display current students
    initializeStudentDb();

    // Bind submit event for student registration form
    $('#frm-register').bind('submit', (e) => {
      e.preventDefault();
      doSubmitForm(e.currentTarget);
    })
  }

  ////// ////// ////// ////// ////// ////// //////
  ////// Implementation
  ////// ////// ////// ////// ////// ////// //////


  function initializeStudentDb() {

    students = studentDbService.getAllStudents();

    if(!students || students.length <= 0) {
      // ajax.loadData(((respStudents) => {
      ajax.loadDataNonJquery((respStudents) => {
          students = respStudents;
          studentDbService.addStudents(students);
          students.forEach(ui_addStudentToList);
        },
        // OnError
        logger.error.bind({}, 'Failed to load data.'));
    } else {
      students.forEach(ui_addStudentToList);
    }

    // Demonstration of using Fetch api (wait 1 second)
    // setTimeout(function() {
    //   logger.info('Demonstrate Loading json using Fetch API.');
    //   ajax.loadDataUsingFetch()
    //     .then(logger.info)
    //     .catch(logger.error);
    // }, 1000);
  }

  /**
   * Handle form submitting
   * @param form
   */
  function doSubmitForm(form) {

    // Read selection option using raw js
    let sel = form['selType'].value;

    // Read selection option using jquery js
    let sel1 = $(form['selType']).val();

    if(!form.checkValidity()) {
      return logger.error(
        'Form is not validated. Please fill in all required fields and try submit again.');
    }

    let student = {
      studentId: form['txtStudentId'].value,
      firstName: form['txtFirstName'].value
    };

    let existStudent = students.filter(s => s.studentId === student.studentId);
    if(existStudent && existStudent.length > 0) {
      alert('Student ID is already taken.');
      return;
    }

    students.push(student);
    studentDbService.addStudent(student);
    ui_addStudentToList(student, true);
    form.reset();
  }

  /**
   * Display new registered student.
   * @param student
   * @param addAnimate
   */
  function ui_addStudentToList(student, addAnimate) {
    let lsStudentElement = $('#lsStudents');

    let studentElement =
      $(`<li class="show student-item list-group-item">
          ${student.studentId} - ${student.firstName}<i class="btn-delete">delete</i></li>`);
    $(lsStudentElement).append(studentElement);

    if(typeof addAnimate === 'boolean' && addAnimate) {
      $(studentElement).addClass('blink-3');
    }

    let btnDelete = $('.btn-delete', studentElement);
    $(btnDelete).bind('click', function() {
      let confirmed = confirm(`Do you want to delete student ${student.firstName} ?`);
      if(!confirmed) return;

      deleteStudent(student, $(this).parent());
    });
  }

  function deleteStudent(student, liElement) {
    studentDbService.deleteStudent(student);
    let idx = students.indexOf(student);
    students.splice(idx, 1);

    $(liElement).fadeOut(300, () => $(this).remove());
  }

  /**
   * Read and display current time
   */
  function readCurrentTime() {
    $('#clock').html(new Date().toString());
    setTimeout(readCurrentTime, 1000);
  }

})();

