<%@ page import="org.apache.jasper.tagplugins.jstl.core.ForEach" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="description" content="CS472-WAP" />
  <meta name="keywords" content="HTML, CSS" />
  
  <title>CS472-WAP-Lesson10 Lab 10 Solution</title>
  <link 
    href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/cosmo/bootstrap.min.css" 
    rel="stylesheet" 
    integrity="sha384-uhut8PejFZO8994oEgm/ZfAv0mW1/b83nczZzSwElbeILxwkN491YQXsCFTE6+nx" 
    crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/page-contact.css">
  <title>CS472 ::: WAP - A JSTL Demonstration</title>
</head>
<body>

<header>
<%@ include file="../fragment/header.jsp" %>
</header>

<section class="page-content container">
  <h2>CS472 ::: WAP - A JSTL Demonstration</h2>
  
  <c:set var="grade" scope="session" value="${15+30+25+10+10}"/>
  
  <c:if test="${grade > 80}">
     <p>My Grade is: <c:out value="${grade}"/><p>
  </c:if>
  
  <c:choose>
      <c:when test="${grade >= 90}">
         You got A.
      </c:when>
      <c:when test="${grade < 90}">
          You did not get A.
      </c:when>
      <c:otherwise>
          Cannot determine grade!
      </c:otherwise>
  </c:choose>
</section>

<footer class="page-footer font-small blue bg-primary">
<%@ include file="../fragment/footer.jsp" %>
</footer>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
</body>
</html>