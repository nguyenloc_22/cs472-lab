<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page  import="java.time.LocalDateTime" %>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="description" content="CS472-WAP" />
  <meta name="keywords" content="HTML, CSS" />

  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
        crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="css/style.css">

  <title>CS472-WAP: Web Application Programming!!!</title>
</head>

<body>
<!-- Something miracle ... & stupid -->
<c:set var="serverTime" value="${ LocalDateTime.now().toString() }"/>

<!-- Header -->
<header class="header">
  <%@ include file="./jsp/fragment/header.jsp" %>
</header>

<!-- Body -->
<div class="page container-fluid">
<ul id="lsMenu">
  <li><a href="/lab9">Lab 9 - Student</a></li>
  <li><a href="/lab10">Lab 10 - Contact Us</a></li>
  <li><a href="/lab11">Lab 11 - Contact Us (jsp)</a></li>
  <li><a href="/lab12">Lab 12 - JSP Custom Tag</a></li>
  <li><a href="/lab13">Lab 13 - Data Access</a></li>
  <li><a href="/task">Project - Task Management</a></li>
  <li><a href="/todo">Lab X - Todo</a></li>
</ul>
</div>
<div class="page container-fluid">
  <h2>Server information</h2>
  Tomcat Version : <%= application.getServerInfo() %><br>
  JSP version : <%= JspFactory.getDefaultFactory().getEngineInfo().getSpecificationVersion() %><br>
  Servered time: ${ serverTime }
</div>

<!-- Footer -->
<footer class="footer">
<%@ include file="./jsp/fragment/footer.jsp" %>
</footer>

<script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>

</body>
</html>
