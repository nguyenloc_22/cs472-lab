Instructions
Create a custom tag, <ct:currentDateTime> that accepts two attributes 
(color and size) that prints the current date and time.

Example:

JSP:  <ct:currentDateTime color="red" size="12px" />

HTML: 
<span style="color: red; font-size: 12px;">
  Mon 2016.04.04 at 04:14:09 PM PDT</span>


You may use the following code snippet:

LocalDate dNow = LocalDate.now();

dNow.format(DateTimeFormatter.ofPattern("EEEE, dd MMMM yyyy"))

System.out.println("Current Date: " + ft.format(dNow));