<%@ page import="org.apache.jasper.tagplugins.jstl.core.ForEach" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ct" uri="WEB-INF/current-date-time.tld"%>


<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/cosmo/bootstrap.min.css" 
    rel="stylesheet" crossorigin="anonymous"
    integrity="sha384-uhut8PejFZO8994oEgm/ZfAv0mW1/b83nczZzSwElbeILxwkN491YQXsCFTE6+nx">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/page-demo.css">
  <title>CS472 ::: WAP - JSP Custom Tag</title>
</head>
<body>

<header>
<%@ include file="./jsp/fragment/header.jsp" %>
</header>

<div class="container">
  <div class="jumbotron">
    <h1 class="display-4">CS472-WAP ::: JSP Custom Tag</h1>
    <p class="lead">This lab implements CurrentDateTime Custom tag.</p>
    <hr class="my-4">
    <p>Create a custom tag, &lt;ct:currentDateTime&gt; that accepts two attributes 
    (color and size) that prints the current date and time.<br/>

    <b>Example:</b>

    <b>JSP:</b> &lt;ct:currentDateTime color="red" size="12px" /&gt;<br/>

    <b>HTML:</b> 
    &lt;span style=&quot;color: red; font-size: 12px;&quot;>Mon 2016.04.04 at 04:14:09 PM PDT&lt;/span&gt;
    </p>
    
    <hr class="my-4">
    <p>
      <ct:currentDateTime color="red" size="18px" />
      <ct:currentDateTime color="green" size="32px" />
      <ct:currentDateTime color="#890" size="12px" />
    </p>
  </div>
</div>

<footer class="page-footer font-small blue bg-primary">
<%@ include file="./jsp/fragment/footer.jsp" %>
</footer>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
</html>