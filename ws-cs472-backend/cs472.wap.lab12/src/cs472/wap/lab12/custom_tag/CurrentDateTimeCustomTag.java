package cs472.wap.lab12.custom_tag;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class CurrentDateTimeCustomTag extends SimpleTagSupport  {
	
	private static DateTimeFormatter dtf;

	private String color;
	private String size;
	
	static {
		dtf = DateTimeFormatter.ofPattern("E, yyyy.MM.dd 'at' HH:mm a G");
	}
	
	public void setColor(String color) {
		this.color = color;
	}
	
	public void setSize(String size) {
		this.size = size;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		super.doTag();
		
		if(color == null || color.isEmpty()) {
			color = "white";
		}
		
		if(size == null || size.isEmpty()) {
			size = "12px";
		}
	       
       JspWriter out = getJspContext().getOut();
       
       String style = String.format("style=\"color: %s; font-size: %s\"", color, size);
       LocalDateTime dNow = LocalDateTime.now();

       String time = dtf.format(dNow);
       out.println(String.format("<span %s>%s</span>", style, time));

	}
	
}
