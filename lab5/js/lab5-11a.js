(function() {

  "use strict";

  setTimeout(main, 1);

  /**
   * 11.	Refer to your work on Lab Assignment 4. Add Javascript code to
   * work with your 2 HTML forms as follows:
   * a.	Login Form: Add code such that when the Submit button is clicked,
   * the values entered in the input fields are printed to the Console.
   *
   * b.	New Product Form: Add code such that when the Submit button is clicked,
   * the values entered in the input fields are displayed in a pop-up window.
   */

  function main() {

    document.getElementById('frm-signup')
    .onsubmit = (e) => {
      e.preventDefault();
      question11APrintSignUpInfo(e.currentTarget);
    }

  }

  /**
   * 11.A:
   * @param form
   * @returns {boolean|void}
   */
  function question11APrintSignUpInfo(form) {

    if(!form.checkValidity()) {
      return console.error(
        'Form is not validated. Please fill in all required fields and try submit again.');
    }

    console.log(`User input:
        Email: ${form['txtEmail'].value},
        Url: ${form['txtUrl'].value},
        Password: ${form['txtPwd'].value}
        isChecked: ${form['chkMeOut'].checked}`);

    return false;
  }

})();
