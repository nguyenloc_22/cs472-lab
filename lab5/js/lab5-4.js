(function() {

  "use strict";

  /**
   * 4.	Define a function sum() and a function multiply()
   * that sums and multiplies (respectively) all the numbers
   * in an input array of numbers. For example, sum([1,2,3,4])
   * should return 10, and multiply([1,2,3,4]) should return 24.
   * Note/Hint: Do these using Imperative programming approach
   * (i.e. for…loop or while…loop)
   */
  function sum(numbers) {
    let sum = 0;
    for(let i = 0; i < numbers.length; i++) {
      sum += numbers[i];
    }

    return sum;
  }

  function multiply(numbers) {
    let sum = 1;
    for(let i = 0; i < numbers.length; i++) {
      sum *= numbers[i];
    }

    return sum;
  }

  console.log(sum([1, 2, 3, 4]));
  // output: 10

  console.log(multiply([1, 2, 3, 4]));
  // output: 24

})();
