(function() {

  "use strict";

  /**
   * 3.	Write a function isVowel() that takes a character
   * (i.e. a string of length 1) and returns true if it is
   * a vowel, false otherwise.
   */
  function isVowel(ch) {
    if(ch.length > 1) {
      return false;
    }

    let rs = false;

    switch(ch) {
      case 'a':
      case 'o':
      case 'u':
      case 'e':
      case 'i':
        rs = true;
        break;
    }

    return rs;
  }

  console.log(isVowel('a'));
  // output: true

})();
