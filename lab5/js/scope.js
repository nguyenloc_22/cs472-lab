(function(){

  var logger = console;
  var x = 10;

  function main() {
    logger.info(`Main Start -> ${x}`);
    var x = 20;

    function f() {
      logger.info(`f -> ${x}`);
      x = 30;
    }

    f();
    logger.info(`Main End -> ${x}`);
  }

  main();

  logger.info(`End -> ${x}`);

})();
