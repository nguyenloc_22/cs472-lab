(function() {

  "use strict";

  /**
   * 1.	Define a function max() that takes two numbers as arguments
   * and returns the largest of them. Use the if-then-else construct
   * available in Javascript.
   */
  function max(num1, num2) {
    if(num1 > num2) {
      return num1;
    } else {
      return num2;
    }
  }

  /**
   * 6.	Write a function findLongestWord() that takes an array of
   * words and returns the length of the longest one.
   */
  function findLongestWord(words) {
    let s = 0;
    for(let i = 0; i < words.length; i++) {
      s = max(s, words[i].length);
    }

    return s;
  }

  console.log(findLongestWord(['hello', 'world', 'ha']));
  // output: 5

})();
