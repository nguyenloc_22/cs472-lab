(function() {

  "use strict";

  setTimeout(main, 1);

  /**
   * 11.	Refer to your work on Lab Assignment 4. Add Javascript code to
   * work with your 2 HTML forms as follows:
   * a.	Login Form: Add code such that when the Submit button is clicked,
   * the values entered in the input fields are printed to the Console.
   *
   * b.	New Product Form: Add code such that when the Submit button is clicked,
   * the values entered in the input fields are displayed in a pop-up window.
   */

  function main() {

    document.getElementById('productForm')
    .onsubmit = (e) => {
      e.preventDefault();
      return question11BShowProductInfo(e.currentTarget);
    }

  }

  /**
   * 11.B:
   * @param form
   * @returns {boolean|void}
   */
  function question11BShowProductInfo(form) {

    // if(!form.checkValidity()) {
    //   return console.error(
    //     'Form is not validated. Please fill in all required fields and try submit again.');
    // }

    let productNb = form['productNumber'].value;
    let productName = form['name'].value;
    let unitPrice = form['unitPrice'].value;
    let quantityInStock = form['quantityInStock'].value;
    let supplier = form['supplier'].value;
    let dateSupplied = form['dateSupplied'].value;

    let text = `User input:
        Product Nb: ${productNb},
        Product Name: ${productName},
        Unit Price: ${unitPrice},
        Quantity In Stock: ${quantityInStock},
        Supplier: ${supplier},
        Date Supplied: ${dateSupplied};
        `;
    alert(text);

    return false;
  }

})();
