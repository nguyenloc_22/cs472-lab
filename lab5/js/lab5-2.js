(function() {

  "use strict";

  /**
   * 2.	Define a function maxOfThree() that takes three numbers as
   * arguments and returns the largest of them.
   */
  function maxOfThree(num1, num2, num3) {
    let max = num1;
    if(max < num2) {
      max = num2;
    }
    if(max < num3) {
      max = num3;
    }

    return max;
  }

  const max1 = (num1, num2) => num1 > num2 ? num1 : num2;
  const maxOfThree1 = (num1, num2, num3) => max1(max1(num1, num2), num3);

  console.log(maxOfThree(1, 2, 3));
  // output: 3

  console.log(maxOfThree1(1, 2, 3));
  // output: 3

})();
