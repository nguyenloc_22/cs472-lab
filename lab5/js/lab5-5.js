(function() {

  "use strict";

  /**
   * 5.	Define a function reverse() that computes the reversal
   * of a string. For example, reverse("jag testar") should
   * return the string "ratset gaj".
   */
  function reverse(text) {
    let rs = [];
    for(let i = text.length-1; i--; i>=0) {
      rs.push(text.charAt(i));
    }

    return rs.join('');
  }

  console.log(reverse('jag testar'));
  // output: 'ratset gaj'

})();
