(function() {

  "use strict";

  /**
   * 10.	Write a function named printFibo, that takes as input, a given
   * length, n, and any two starting numbers a and b, and it prints-out
   * the Fibonacci sequence (0, 1, 1, 2, 3, 5, 8, 13, 21, 34,…) of the
   * given length, beginning with a and b.
   * (e.g.
   *  printFibo(n=1, a=0, b=1), prints-out: "0", as output;
   *  printFibo(n=2, a=0, b=1), prints-out: "0, 1", as output;
   *  printFibo(n=3, a=0, b=1), prints-out: "0, 1, 1", as output;
   *  printFibo(n=6, a=0, b=1), prints-out: "0, 1, 1, 2, 3, 5", as output;
   *  and printFibo(n=10, a=0, b=1), prints-out: "0, 1, 1, 2, 3, 5, 8, 13, 21, 34", as output).
   */
  function printFibo(n, a, b) {

    if(n === 0) return;
    if(n === 1) {
      return console.log(`${a}`);
    }
    if(n === 2) {
      return console.log(`${a}, ${b}`);
    }

    let rs = `${a}, ${b}`;
    let nextA = a, nextB = b;

    for(let i = 0; i < n-2; i++) {
      let next = nextA + nextB;
      nextA = nextB;
      nextB = next;

      rs += `, ${next}`;
    }

    console.log(rs);
  }

  printFibo(1, 0, 1);
  // prints-out: "0", as output;
  printFibo(2, 0, 1);
  // prints-out: "0, 1", as output;
  printFibo(3, 0, 1);
  // prints-out: "0, 1, 1", as output;
  printFibo(6, 0, 1);
  // prints-out: "0, 1, 1, 2, 3, 5", as output;
  printFibo(10, 0, 1);
  // prints-out: "0, 1, 1, 2, 3, 5, 8, 13, 21, 34", as output).


})();
