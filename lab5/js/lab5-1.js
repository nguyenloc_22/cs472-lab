(function() {

  "use strict";

  /**
   * 1.	Define a function max() that takes two numbers as arguments
   * and returns the largest of them. Use the if-then-else construct
   * available in Javascript.
   */
  function max(num1, num2) {
    if(num1 > num2) {
      return num1;
    } else {
      return num2;
    }
  }

  const max1 = (num1, num2) => num1 > num2 ? num1 : num2;

  console.log(max(1, 2));
  // output: 2

  console.log(max1(1, 2));
  // output: 2

})();
