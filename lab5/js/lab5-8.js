(function() {

  "use strict";

  /**
   * 8.	Using the Array.reduce(…) function, re-implement your
   * functions, sum(…) and multiply(…) (defined in Problem 4
   * above) without using Imperative programming. i.e. Do NOT
   * use any explicit looping construct; instead use functional
   * programming style/approach.
   */
  function sumFn(numbers) {
    return numbers.reduce((n1, n2) => n1 + n2, 0);
  }

  function multiplyFn(numbers) {
    return numbers.reduce((n1, n2) => n1 * n2, 1);
  }

  console.log(sumFn([1, 2, 3, 4]));
  // output: 10

  console.log(multiplyFn([1, 2, 3, 4]));
  // output: 24

})();
