(function() {

  "use strict";

  /**
   * 7.	Write a function filterLongWords() that takes an array of
   * words and an integer i and returns a new array containing only
   * those words that were longer than i characters.
   */
  function filterLongWords(words, i) {
    return words.filter(w => w.length > i);
  }

  console.log(filterLongWords(['Hello', 'world', 'ha'], 3));
  // output: ['Hello', 'world']


})();
