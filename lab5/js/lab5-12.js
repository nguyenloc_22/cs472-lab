(function() {

  "use strict";

  setTimeout(main, 1);

  /**
   * 12.	Using JavaScript and HTML and CSS, implement a webpage that displays
   * a working, ticking counter Clock, that counts/displays the current Date
   * and time of the browser host, in the format: 2019-11-4 12:16:01
   */
  function main() {
    readTime();
  }

  function readTime() {

    let n = new Date();
    let t = `${n.getFullYear()}-${n.getMonth()+1}-${n.getDate()} ${n.getHours()}:${n.getMinutes()}:${n.getSeconds()}`;

    document.getElementById('clock').innerHTML = t;

    setTimeout(readTime, 1000);
  }


})();
