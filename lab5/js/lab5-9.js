(function() {

  "use strict";

  /**
   * 9.	Implement Javascript code for a function named, findSecondBiggest,
   * which takes as input, an array of numbers and finds and returns the
   * second biggest of the numbers. For example, findSecondBiggest([1,2,3,4,5])
   * should return 4. And findSecondBiggest([19,9,11,0,12]) should return 12.
   * (Note: Do not use sorting!)
   */
  function findSecondBiggest(numbers) {
    if(numbers.length < 1) {
      throw new Error('Size is invalid.');
    }

    let biggest = -Infinity,
      next_biggest = -Infinity;

    for (let i = 0, n = numbers.length; i < n; ++i) {

      if (numbers[i] > biggest) {
        next_biggest = biggest;
        biggest = numbers[i];
      } else if (numbers[i] < biggest && numbers[i] > next_biggest) {
        next_biggest = numbers[i];
      }
    }

    return next_biggest;
  }

  console.log(findSecondBiggest([1, 2, 3, 4, 5]));
  // output: 4

  console.log(findSecondBiggest([19, 9, 11, 0, 12]));
  // output: 12

})();
