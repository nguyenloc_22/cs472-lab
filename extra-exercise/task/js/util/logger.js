class Logger {

  constructor(tag) {
    this.tag = tag;
  }

  // return {
  //   debug: debug,
  //   info: info,
  //   warn: warn,
  //   error: error
  // };

  debug(msg) {
    console.log(`[debug][${this.tag}] ${msg}`);
  }

  info(msg) {
    console.info(`[info][${this.tag}] ${msg}`);
  }

  warn(msg, err) {
    console.warn(`[warn][${this.tag}] ${msg}`, err);
  }

  error(msg, err) {
    console.error(`[error][${this.tag}] ${msg}`, err);
  }

}
