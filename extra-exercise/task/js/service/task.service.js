function TaskService() {
  this.logger = new Logger('task-service');
  this.createdTime = new Date();
}

TaskService.prototype.getAllTask = getAllTask;
TaskService.prototype.getTaskById = getTaskById;

function getAllTask() {
  this.logger.debug('getAllTask. Called at ' + this.createdTime.toTimeString());
}

function getTaskById(taskId) {
  this.logger.debug('getTaskById. Called at ' + this.createdTime.toTimeString());
  return {
    id: taskId,
    name: 'Demo Task',
    description: 'Simple description'
  };
}
