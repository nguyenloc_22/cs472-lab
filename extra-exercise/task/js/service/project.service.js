class ProjectService {

  constructor() {
    this.logger = new Logger('project-service');
    this.createdTime = new Date();
  }

  getAllProjects() {
    console.log('getAllProjects. Called at ' + this.createdTime.toTimeString());
  }
}
/*

function ProjectService() {
  this.logger = new Logger('project-service');
  this.createdTime = new Date();
}

ProjectService.prototype.getAllProjects = getAllProjects;
ProjectService.prototype.getProjectById = getProjectById;

function createProject(name, description) {

}

function getAllProjects() {
  console.log('getAllProjects. Called at ' + this.createdTime.toTimeString());
}

function getProjectById(projectId) {
  console.log('getProjectById. Called at ' + this.createdTime.toTimeString());
  return {
    id: projectId,
    name: 'Demo Project',
    email: 'demo@gmail.com'
  };
}

export let projectService = new ProjectService();
*/
