function UserService() {
  this.logger = new Logger('user-service');
  this.createdTime = new Date();
}

UserService.prototype.getAllUsers = getAllUsers;
UserService.prototype.getUserById = getUserById;

function getAllUsers() {
  console.log('getAllUsers. Called at ' + this.createdTime.toTimeString());
}

function getUserById(userId) {
  console.log('getUserById. Called at ' + this.createdTime.toTimeString());
  return {
    id: userId,
    name: 'Demo User',
    email: 'demo@gmail.com'
  };
}
