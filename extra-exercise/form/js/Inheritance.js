
function Question10() {

  var name = 'aName';

  function f1() {}
  function setName(n) { name = n; }

  return function (){
    this.setName = setName;
  }

}

Question10.prototype.address = '';

Question10.prototype.setAddress = function(add) {
  this.address = add;
};



var q = new Question10();

q.setAddress('Here');
console.log('Address -> ' + q.address);
