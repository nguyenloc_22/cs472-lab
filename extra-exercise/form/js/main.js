class App {

  logger: Logger;

  constructor() {
    this.logger = new Logger('main');
    this.logger.log('main > constructor');
  }

  foo() {
    this.logger.log('main > foo');
  }
}
