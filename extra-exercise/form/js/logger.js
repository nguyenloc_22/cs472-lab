class Logger {
  constructor(tag) {
    this.tag = tag;
  }

  log(msg) {
    console.log(`[log][${this.tag}] ${msg}`);
  }
}
