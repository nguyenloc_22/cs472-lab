"use strict";

import { Person } from './person.js';

export class Student extends Person {
    constructor(id, name, age) {
        super(name, age);
        this.id = id;
    }

    print1() {
        console.log(`Student { Id: ${this.id}, Name: ${this.name}, Age: ${this.age} }`);
    }

}
