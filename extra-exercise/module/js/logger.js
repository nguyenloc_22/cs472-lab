"use strict";

export class Logger {

    static loggers = {};
    _logger = console;

    constructor(name) {
        this.name = name;
    }

    static get(name) {
        if(!name) {
            name = 'default'
        }

        let l = this.loggers[name];
        if(!l) {
            l = new Logger(name);
            this.loggers[name] = l;
        }

        return l;
    }

    debug(msg) {
        this._logger.debug(`[debug] ${msg}`);
    }

    info(msg) {
        this._logger.info(`[info] ${msg}`);
    }

    warn(msg, err) {
        this._logger.warn(`[warn] ${msg}`, err);
    }

    error(msg, err) {
        this._logger.error(`[error] ${msg}`, err);
    }

}
