"use strict";

/* Import */
import { Student } from './student.js';
import { Logger } from './logger.js';
/* Constant */
const logger = Logger.get('app');

class Main {

  constructor() {

    logger.info('Loading application.');

    let eugene = new Student(1, 'Eugene J Rautenbach', 62);
    eugene.print1();
  }
}

new Main();


