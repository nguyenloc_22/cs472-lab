"use strict";

export class Person {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }

    print() {
        console.log(`Person { Name: ${this.name}, Age: ${this.age} }`);
    }
}
