(function() {

  "use strict";

  const logger = console;

  setTimeout(main, 1);

  function main() {

    // test001();
    test002();
  }

  function test002() {
    let me = new Person('locnv', 30);
    let meAsStudent = new Student(1001, 'locnv', 32);

    me.print();
    meAsStudent.print();
    meAsStudent.print1();
  }

  function test001() {
    let defPerson = new Person();

    let john = Object.create(defPerson);
    john.name = 'John';
    john.age = 34;
    john.print();

    let tony = Object.create(defPerson);
    tony.name = 'Tony';
    tony.age = 62;
    tony.print();
  }

  function Person() {
    return {
      name: '',
      age: 0,
      print: function() {
        console.log(`Person: Name -> ${this.name} : Age -> ${this.age}`);
      }
    };
  }



})();
