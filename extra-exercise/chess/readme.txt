http://code2care.org/pages/chessboard-with-pieces-using-pure-html-and-css/

1. White Chess King ♔

Unicode : U+2654
HTML Code : &#9812;


2. Black Chess King ♚

Unicode : U+265A
HTML Code : &#9818;


3. White Chess Queen ♕

Unicode : U+2655
HTML Code : &#9813;


4. Black Chess Queen ♛

Unicode : U+265B
HTML Code : &#9819;


5. White Chess Rock ♖

Unicode : U+2656
HTML Code : &#9814;


6. Black Chess Rock ♜

Unicode : U+265C
HTML Code : &#9820;


7. White Chess Bishop ♗

Unicode : U+2657
HTML Code : &#9815;


8. Black Chess Bishop ♝

Unicode : U+265D
HTML Code : &#9821;


9. White Chess Knight ♘

Unicode : U+2658
HTML Code : &#9816;


10. Black Chess Knight ♞

Unicode : U+265E
HTML Code : &#9822;


11. White Chess Pawn ♙

Unicode : U+2659
HTML Code : &#9817;


11. Black Chess Pawn ♟

Unicode : U+265F
HTML Code : &#9823;




Source Code HTML + CSS
