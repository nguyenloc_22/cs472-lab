(function() {

  "use strict";

  const logger = console;
  const BoardWidth = 8;
  const BoardHeight = 8;
  const Size = BoardWidth * BoardHeight;
  const Open = 1;
  const Close = 0;

  const Rook = 8;
  const Queen = 9;
  const King = 10;

  const Board = Array(BoardWidth*BoardHeight).fill(0);

  setTimeout(main, 1);

  function main() {
    initializeBoard();

    d_printBoard();
  }

  function initializeBoard() {
    logger.debug('Initialize Board.');

    let state = Open;
    for(let i = 0; i < Size; i++) {
      Board[i] = state;
      if(i%8 === 0) {
        state = 1 - state;
      }
    }

    let boardContainer = document.getElementById('board');

    // let size =
  }

  function d_printBoard() {
    let t = '';

    for(let r = 0; r < BoardHeight; r++) {
      for(let c = 0; c < BoardWidth; c++) {
        t += Board[c];
        if(c < BoardWidth-1) {
          t += ' ';
        }
      }
      t += '\n';
    }

    logger.debug(t);
  }

})();
