
(function() {

  const logger = console;

  setTimeout(main, 1);

  function main() {
    this.name = 'Grandpa';
    logger.info('Hello Javascript!');

    foo();

    let stranger = {
      name: 'Thief!'
    };
    stranger.stolenFn = foo;
    stranger.stolenFn();
  }

  function foo() {
    logger.info(this.name);
  }

  function a() {
    var x;
    console.log(`a -> ${x}`);
  }

  function b() {
    var x = 20;
    console.log(`b -> ${x}`);
    a();
  }

  var x = 30;
  console.log(`global -> ${x}`);

  b();

})();
