Lab 6: Report

1. Implement web page to register a student (using bootstrap for style)
2. Using Unobstrusive style to bind onsubmit event for form
3. Binding data from js (json data array) onto html.
4. Dynamic update form element using document.createElement(...) & listObjectRef.appendChild(...),
5. Some extra practicing with css3 animation
