(function() {

  "use strict";

  // Constants
  const logger = console;

  // Variables
  let students = [
    { "studentId": "000-98-0001", "firstName": "James" },
    { "studentId": "000-98-0002", "firstName": "Anna" },
    { "studentId": "000-98-0003", "firstName": "Jeffrey" }
  ];


  setTimeout(main, 1);

  function main() {
    logger.info('CS472-WAP -> Lab 6');

    // Update current time
    readCurrentTime();

    // Display current students
    students.forEach(ui_addStudentToList);

    // Bind onSubmit event for Registration form
    document.getElementById('frm-register')
    .onsubmit = (e) => {
      e.preventDefault();
      doSubmitForm(e.currentTarget);
      return false;
    }
  }

  /**
   * Handle form submitting
   * @param form
   */
  function doSubmitForm(form) {

    if(!form.checkValidity()) {
      return logger.error(
        'Form is not validated. Please fill in all required fields and try submit again.');
    }

    let student = {
      studentId: form['txtStudentId'].value,
      firstName: form['txtFirstName'].value
    };

    let existStudent = students.filter(s => s.studentId === student.studentId);
    if(existStudent && existStudent.length > 0) {
      alert('Student ID is already taken.');
      return;
    }

    students.push(student);
    ui_addStudentToList(student);
    form.reset();
  }

  /**
   * Display new registered student.
   * @param student
   */
  function ui_addStudentToList(student) {
    let lsStudentElement = document.getElementById('lsStudents');

    let studentElement = document.createElement('li');
    studentElement.setAttribute('class','student-item list-group-item');

    lsStudentElement.appendChild(studentElement);
    studentElement.innerHTML = `${student.studentId} - ${student.firstName}`;
  }

  /**
   * Read and display current time
   */
  function readCurrentTime() {
    document.getElementById('clock').innerHTML = new Date().toString();
    setTimeout(readCurrentTime, 1000);
  }

})();
