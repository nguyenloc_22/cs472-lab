"use strict";

function Employee() {
  let name, age, salary;

  return {
    setAge: setAge,
    setSalary: setSalary,
    setName: setName,
    increaseSalary: increaseSalary,
    increaseAge: increaseAge,
    print: print
  };

  function print() {
    console.log(`Employee: 
    Name: ${name}
    Age: ${age}
    Salary: ${salary}`);
  }

  function setAge(nAge) {
    age = nAge;
  }

  function setSalary(nSalary) {
    salary = nSalary;
  }

  function setName(nName) {
    name = nName;
  }

  function increaseSalary(percentage) {
    let currentSalary = getSalary();
    let newSalary = percentage * currentSalary + currentSalary;

    this.setSalary(newSalary);
  }

  function increaseAge() {
    this.setAge(1 + getAge());
  }

  function getAge() {
    return age;
  }

  function getSalary() {
    return salary;
  }

  function getName() {
    return name;
  }
}
