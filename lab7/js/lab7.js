/**
 * 6. Consider the following definition of an add() function to increment a counter variable:
 *
 * var add = (function() {
 *   var counter = 0;
 *   return function() {
 *     return counter += 1;
 *   }
 * })();
 *
 * Modify the above module to define a count object with two methods:
 * - add() --> adds one to the counter (as above).
 * - reset() --> sets the counter to 0
 *
 * 7. In the definition of add() shown in question 6, identify the "free" variable.
 * In the context of a function closure, what is "free variable"
 *
 * * ANSWER:
 * Free variable: counter
 * What is free variable: it is the variable that is being used by a closure that
 * neither be declared locally with that closure scope nor passed as parameters
 *
 * 8. The add() function defined in question 6 always adds 1 to the counter each time it is called.
 * Write a definition of a function make_adder(inc), whose return value is an add function with
 * increment value inc (instead of 1). Here is an example of using this funciton:
 *
 * add5 = make_adder(5);
 * add5();
 * add5();
 * add5();
 * // -> final counter value is 15
 *
 * add7 = make_adder(7);
 * add7();
 * add7();
 * add(7);
 * // -> final counter value is 21
 */

(function() {

  "use strict";

  // Constants
  const print = console.info;

  // Variables

  setTimeout(main, 1);

  function main() {
    print('CS472-WAP -> Lab 7');

    // Update current time
    readCurrentTime();

    let counterFn = Counter();

    print(counterFn.add()); // -> 1
    print(counterFn.add()); // -> 2

    print(counterFn.reset()); // -> 0

    let add5 = make_adder(5);
    print(add5()); // --> 5
    print(add5()); // --> 10
    print(add5()); // --> 15

    let add7 = make_adder(7);
    print(add7()); // --> 7
    print(add7()); // --> 14
    print(add7()); // --> 21

    // 11. Instantiate Employee
    // -> No address & setAddress Fn
    // -> Extend (prototyping) to add new address property and setAddress Fn
    // on the Employee interface (module)
    // Instantiate second Employee
    // --> new property 'address' and setAddress Fn is available on new instance.


    let employee = new Employee();
    employee.setName('John');
    employee.setAge(62);
    employee.setSalary(150000);

    employee.print();


    // Prototyping / extend

    Employee().__proto__.address = '';
    Employee().__proto__.setAddress = function (add) {  this.address = add };
    Employee().__proto__.getAddress = function () {  return this.address };

    // New instance

    let extendEmployee = new Employee();
    extendEmployee.setName('Tom');
    extendEmployee.setAge(60);
    extendEmployee.setSalary(120000);

    extendEmployee.print();

    extendEmployee.setAddress('1000 4th, FairField IA');
    print(extendEmployee.getAddress());
  }

  function Counter() {

    let counter = 0;

    return {
      add: add,
      reset: reset
    };

    function add() {
      return counter += 1;
    }

    function reset() {
      counter = 0;
      return counter;
    }
  }

  function make_adder(inc) {
    let counter = 0;
    return function() {
      return counter += inc;
    }
  }

  /**
   * Read and display current time
   */
  function readCurrentTime() {
    document.getElementById('clock').innerHTML = new Date().toString();
    setTimeout(readCurrentTime, 1000);
  }

})();
