Instructions
In this lab/homework assigment 6, you are going to build a basic "Student Registration form",
which is a simple client-side web application and deploy it to your website at http://mumstudents.org
or on netlify.

Your application UI should look similar to the attached image (W2D2WebApp_Screenshot).
But you are free to add any further enhancements you like. It should then function as follows:

1. Every user (i.e. the Student who wants to register in the system) must provide their
Student ID and First Name.

2. When they click the submit button, the application should capture the data their information
and display it, added to a List presented at the bottom of the Registration Form, as shown.

3. When the application is first loaded to the Web browser, it should display the following list
of existing student JSON formated data:

{ "studentId": "000-98-0001", "firstName": "James" },
{ "studentId": "000-98-0002", "firstName": "Anna" },
{ "studentId": "000-98-0003", "firstName": "Jeffrey" }

Your implementation should have/use the following features:

1. IIFE - Use an IIFE to initialize the state of the web page. i.e. wrap your JavaSscript code
which contains your event handlers etc, inside an IIFE.

2. Use Unobstrusive JS for Event handler - i.e. use the DOM API call such as, objectRef.addEventListener,
to create your event handlers.

3. Store your startup data as a JSON array e.g. const students = [ { "000-98-0001": "John" }, {"...":"..."} ];

4. Use the DOM API calls, document.createElement(...) & listObjectRef.appendChild(...),
to dynamically add new Student data to the List Items on the webpage.

5. Use Bootstrap (or some other CSS framework you prefer) to style/present the UI of your app
to appear like the sample or similar.

When finished and tested all okay, Upload your source code here to Sakai as a single .html file
or create a zip file and upload (if it is not a single file).

Make sure to add a brief note, saying what you were able to do, how you did it and how it went,
what worked and what didn't quite work.

Enjoy!
