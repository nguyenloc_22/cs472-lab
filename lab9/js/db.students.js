"use strict";

const DbName = 'cs472-db-student';
const logger = console;

/**
 *
 * @constructor
 */
function StudentDbService() {
  this.localStorage = new LocalStorage();
}

// Prototyping

StudentDbService.prototype.addStudent = addStudent;
StudentDbService.prototype.addStudents = addStudents;
StudentDbService.prototype.deleteStudent = deleteStudent;
StudentDbService.prototype.getAllStudents = getAllStudents;
StudentDbService.prototype.dropDb = dropDb;

// Implementation


function addStudent(student) {
  let students = this.localStorage.getObject(DbName);
  if(!students) {
    students = [];
  }

  if(!Array.isArray(students)) {
    logger.warn(`Current stored students is not an array. Data is going to be reset ...`);
    logger.warn(students);

    students = [];
  }

  students.push(student);

  this.localStorage.saveObject(DbName, JSON.stringify(students));
}

function addStudents(students) {
  students.forEach(this.addStudent.bind(this));
}

function deleteStudent(student) {
  let allStudents = this.getAllStudents();
  if(!allStudents || allStudents.length <= 0) {
    return; // No student to delete
  }

  let studentToDelete = null;
  for(let i = 0; i < allStudents.length; i++) {
    if(allStudents[i].studentId === student.studentId) {
      studentToDelete = allStudents[i];
      break;
    }
  }

  if(!studentToDelete) {
    logger.warn(`Failed to delete student ${student.name}. Student not found.`);
    return;
  }

  logger.warn(`You are about deleting student -> ${student.studentId}`);
  let idx = allStudents.indexOf(studentToDelete);
  if(idx !== -1) {
    allStudents.splice(idx, 1);
    this.dropDb();
    this.addStudents(allStudents);
  }

  // let studentsToDelete = allStudents.filter((s) => s.studentId === student.studentId);
  // if(studentsToDelete) {
  //   studentsToDelete.forEach((s) => {
  //     let idx = allStudents.indexOf(s);
  //     if(idx !== -1) {
  //       allStudents.splice(idx, 1);
  //     }
  //   });
  //
  //   this.dropDb();
  //   this.addStudents(allStudents);
  // }

}

function getAllStudents() {
  let students = this.localStorage.getObject(DbName);
  if(!Array.isArray(students)) {
    logger.warn(`[db.student] getAll -> students is not an array object.`);
    return [];
  }
  return students;
}

function dropDb() {
  this.localStorage.remove(DbName);
}
