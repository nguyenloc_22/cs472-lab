"use strict";

/**
 * Implementation a Simple storage service using web localStorage API
 *
 * Apply Prototyping Pattern
 */

/**
 *
 * @constructor
 */
function LocalStorage() {
  this.storage = window.localStorage;
}

// Function prototyping

LocalStorage.prototype.saveObject = saveObject;
LocalStorage.prototype.getString = getString;
LocalStorage.prototype.getObject = getObject;
LocalStorage.prototype.remove = remove;


// Implementation

function saveObject(key, value) {
  this.storage.setItem(key, value);
}

function getString (key) {
  return this.storage.getItem(key);
}

function getObject(key) {
  let obj = null;
  let text = this.storage.getItem(key);
  if(text) {
    obj = JSON.parse(text);
  }

  return obj;
}

function remove(key) {
  this.storage.removeItem(key);
}
