Instructions
In this Lab assignment, you will take your Student Registration AJAX Single Page WebApp from Lab8 and add code to implement Client-side Data Persistence using LocalStorage API. This functionality will enable your webapp to do a kind of Data caching and be able to work offline.

When finished, re-deploy this new version of your webapp to your mumstudents.org website or to netlify.

Make a submission here on Sakai, either with attaching zipfile containing your webapp's source-code or just the url to the code in your github repo or elsewhere.

Enjoy!
