# CS472 Lab & Homework

**Lab 1: Home page**  
**Lab 2: About me**  
**Lab 3: BBC page layout**  
Instructions
The attached image (bbc_dot_com.png) is a section of the homepage of a popular public website (www.bbc.co.uk). In this assignment lab3, implement code to display a webpage that shows content to appear like the layout of the content of the image.

Take a screenshot of your webpage and save it in a .png file. Then upload both your screenshot and a Zipfile of your project-folder.

Enjoy!

**Lab 4: Forms**

For this Lab Assignment 4, do the following tasks:
1. Implement code for a web page the displays the attached Web Form (file named, webform1.PNG).
2. In your html form markup code from task 1, include a feature such that the User can only submit the form if the Password field is at least 10 characters in length and contain at least one number and one uppercase and lowercase letter).
3. Add to your above web form, an additional text input field, where the user must only enter a website url, and they must enter it beginning with either the string, "http://" or "https://".
4. Implement code for another web page the displays the attached Web Form (file named, Webform2.PNG).

**CDN ref**
1. Bootstrap  
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
2. Jquery  
3. Font Awesome


## Form
Pattern
 --> Phone, email, xxx
