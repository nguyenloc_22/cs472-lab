(function() {

  "use strict";

  // Constants
  const logger = console;

  let students = [];
  let ajax = new Ajax();


  setTimeout(main, 1);

  function main() {
    logger.info('CS472-WAP -> Lab 8');

    // Update current time
    readCurrentTime();

    // Display current students
    // ajax.loadData(((respStudents) => {
    ajax.loadDataNonJquery((respStudents) => {
      students = respStudents;
      students.forEach(ui_addStudentToList);
    },
    // OnError
    logger.error.bind({}, 'Failed to load data.'));


    // Demonstration of using Fetch api (wait 1 second)
    setTimeout(function() {
      logger.info('Demonstrate Loading json using Fetch API.');
      ajax.loadDataUsingFetch()
      .then(logger.info)
      .catch(logger.error);
    }, 1000);

    $('#frm-register').bind('submit', (e) => {
      e.preventDefault();
      doSubmitForm(e.currentTarget);
    });
  }

  /**
   * Handle form submitting
   * @param form
   */
  function doSubmitForm(form) {

    if(!form.checkValidity()) {
      return logger.error(
        'Form is not validated. Please fill in all required fields and try submit again.');
    }

    let student = {
      studentId: form['txtStudentId'].value,
      firstName: form['txtFirstName'].value
    };

    let existStudent = students.filter(s => s.studentId === student.studentId);
    if(existStudent && existStudent.length > 0) {
      alert('Student ID is already taken.');
      return;
    }

    students.push(student);
    ui_addStudentToList(student);
    form.reset();
  }

  /**
   * Display new registered student.
   * @param student
   */
  function ui_addStudentToList(student) {
    let lsStudentElement = $('#lsStudents');

    let studentElement =
      $(`<li class="show student-item list-group-item">${student.studentId} - ${student.firstName}</li>`);
    $(lsStudentElement).append(studentElement);
  }

  /**
   * Read and display current time
   */
  function readCurrentTime() {
    $('#clock').html(new Date().toString());
    setTimeout(readCurrentTime, 1000);
  }

})();
