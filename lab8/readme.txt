Instructions
In this lab assignment, you will be making a simple kind of Single Page App (aka SPA).
We will start with the content and functionality which you have already implemented in
our simple Student Registration form app (from W2D2 Lab6 lab assignment).
You will now enhance that app, by doing the following:

(Note: A finished/working solution for the lab6 Student Registration webapp is available
on the Labs folder in Sakai++. So, you may download and use this version, if yours isn't
working correctly)

1. Convert it from using plain-vanilla DOM API functions to using JQuery API functions
(i.e. add the JQuery library and use it to "JQueryfy" the app).

2. AJAXify the app by replacing the hard-coded students data with an AJAX remote call to
retrieve the data from our web server. e.g. Once the webpage loads, we will fetch/read/load
the students data from a server url like "http://localhost/ourAppName/data/studentsData.json".

3. On receiving the data from the server url (i.e. the backend), we will then present/add
it to our screen, inside the Students list that already exist on our webpage.

If you finish the above tasks early, for additional jQuery practice, feel free to do the exercises
available at: http://try.jquery.com/

For additional knowledge, implement the following 2 other alternative ways to achieve the AJAXified
functionality:

1. Use the XMLHttpRequest object

2. Use the Fetch API
