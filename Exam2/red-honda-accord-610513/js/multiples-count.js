(function () {

  "use strict";

  const print = console.log;

  const multiplesCount = (nums, b) => nums.reduce((count, num) => count += (num % b === 0) ? 1 : 0, 0);

  print('\n -> Multiple Count:');
  print(multiplesCount([1, 2, 3, 4, 5, 6], 3));
  print(multiplesCount([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15], 5));

})();
