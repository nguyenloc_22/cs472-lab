(function () {

  "use strict";

  const print = console.log;

  const calculateAverage = function (...numbers) {
    let sum = numbers.reduce((n1, n2) => n1 + n2, 0);
    return sum / numbers.length;
  };

  print('\n -> Calculate Average:');
  print(calculateAverage(1,2,3));
  print(calculateAverage(1,2,3, 4));
  print(calculateAverage(1,2,3, 4, 5, 16.6));

})();
