(function () {

  "use strict";

  const print = console.log;

  // 2.1 Define a base object names, Car.
  const Car = {
    make: 'default',
    model: 'default',
    color: 'default',

    drive: function(speed) {
      print(`The ${this.color} ${this.make} ${this.model} is driving at ${speed} mph.`);
    },

    stop: function() {
      print(`The ${this.color} ${this.make} ${this.model} is stopping.`);
    }
  };

  const RedHondaAccord = Object.create(Car);

  RedHondaAccord.color = 'red';
  RedHondaAccord.make = 'Honda';
  RedHondaAccord.model = 'Accord';

  RedHondaAccord.drive(200);

  // 2.2

})();
