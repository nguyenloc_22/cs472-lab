(function () {

  "use strict";

  const print = console.log;

  function Car(make, model, color) {
    this.make = make || 'default';
    this.model = model || 'default';
    this.color = color || 'default';
  }

  Car.prototype.drive = function(speed) {
    print(`The ${this.color} ${this.make} ${this.model} is driving at ${speed} mph.`);
  };

  Car.prototype.stop = function() {
    print(`The ${this.color} ${this.make} ${this.model} is stopping.`);
  };

  const RedHondaAccord = new Car('Honda', 'Accord', 'red');

  RedHondaAccord.drive(300);

  // 2.2

})();
