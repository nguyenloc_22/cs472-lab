"use strict";

const DbName = 'cs472-db-mini-bank';
const logger = console;

/**
 *
 * @constructor
 */
function CustomerDbService() {
  this.localStorage = new LocalStorage();
}

// Prototyping

CustomerDbService.prototype.addCustomer = addCustomer;
CustomerDbService.prototype.addCustomers = addCustomers;
CustomerDbService.prototype.deleteCustomer = deleteCustomer;
CustomerDbService.prototype.getAllCustomers = getAllCustomers;
CustomerDbService.prototype.dropDb = dropDb;

// Implementation


function addCustomer(customer) {
  let customers = this.localStorage.getObject(DbName);
  if(!customers) {
    customers = [];
  }

  if(!Array.isArray(customers)) {
    logger.warn(`Current stored customers is not an array. Data is going to be reset ...`);
    logger.warn(customers);

    customers = [];
  }

  customers.push(customer);

  this.localStorage.saveObject(DbName, JSON.stringify(customers));
}

function addCustomers(customers) {
  customers.forEach(this.addCustomer.bind(this));
}

function deleteCustomer(customer) {
  let allCustomers = this.getAllCustomers();
  if(!allCustomers || allCustomers.length <= 0) {
    return; // No customer to delete
  }

  let customerToDelete = null;
  for(let i = 0; i < allCustomers.length; i++) {
    if(allCustomers[i].accountNo === customer.accountNo) {
      customerToDelete = allCustomers[i];
      break;
    }
  }

  if(!customerToDelete) {
    logger.warn(`Failed to delete customer ${customer.customerName}. Customer not found.`);
    return;
  }

  logger.warn(`You are about deleting customer -> ${customer.accountNo}`);
  let idx = allCustomers.indexOf(customerToDelete);
  if(idx !== -1) {
    allCustomers.splice(idx, 1);
    this.dropDb();
    this.addCustomers(allCustomers);
  }

}

function getAllCustomers() {
  let customers = this.localStorage.getObject(DbName);
  if(!Array.isArray(customers)) {
    logger.warn(`[db.customer] getAll -> customers is not an array object.`);
    return [];
  }
  return customers;
}

function dropDb() {
  this.localStorage.remove(DbName);
}
