function Ajax() {

  const logger = console;
  const StudentsDataPath = 'data/customerData.json';

  return {
    // Loading json using jquery
    loadData: loadData,

    // Loading json using xhr
    loadDataNonJquery: loadDataNonJquery,

    // Loading json using js fetch api
    loadDataUsingFetch: loadDataUsingFetch

  };

  // Default loadData function using jQuery
  function loadData(onLoaded, onError) {

    $.getJSON(StudentsDataPath)
    .done(onLoaded)
    .fail((err) => {
      logger.log('[ajax] Failed to load data', err);
      onError(err);
    })
    .always(logger.log.bind({},'[ajax] Request done.'));
  }

  /**
   * Load data using
   */
  function loadDataNonJquery(onLoaded, onError) {
    let xhr = new XMLHttpRequest();

    xhr.open('get', StudentsDataPath);

    xhr.onreadystatechange = function() {

      logger.debug(`[ajax][xhr] OnReadyStateChange. ReadyState -> ${this.readyState} & Status -> ${this.status}`);

      if (this.readyState === 4 && this.status === 200) {
        let students = JSON.parse(this.responseText);
        onLoaded(students);
      }
    };

    xhr.addEventListener('error', onError);
    xhr.send();
  }

  async function loadDataUsingFetch(onLoaded, onError) {

    const response = await fetch(StudentsDataPath);
    const students = await response.json();
    logger.info('Fetch done.', students);
  }
}
