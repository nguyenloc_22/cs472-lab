(function() {

  "use strict";

  // Constants
  const logger = console;
  const customerDbService = new CustomerDbService();

  // Local variables
  let customers = null;
  let ajax = new Ajax();

  // TODO for test only
  // customerDbService.dropDb();

  // Schedule to start app's entry point.
  setTimeout(main, 1);

  // Entry point
  function main() {
    logger.info('CS472-WAP -> Mini Bank (Exam #2)');

    // Display current customers
    initializeCustomerDb();

    // Bind submit event for customer registration form
    $('#frm-register').bind('submit', (e) => {
      e.preventDefault();
      doSubmitForm(e.currentTarget);
    });

    // Bootstrap form validation
    let forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    let validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }

  ////// ////// ////// ////// ////// ////// //////
  ////// Implementation
  ////// ////// ////// ////// ////// ////// //////


  function initializeCustomerDb() {

    customers = customerDbService.getAllCustomers();

    if(!customers || customers.length <= 0) {
      ajax.loadDataNonJquery((respCustomers) => {
          customers = respCustomers;
          customerDbService.addCustomers(customers);
          customers.forEach(ui_addCustomerToList);
        },
        // OnError
        logger.error.bind({}, 'Failed to load data.'));
    } else {
      customers.forEach(ui_addCustomerToList);
    }
  }

  /**
   * Handle form submitting
   * @param form
   */
  function doSubmitForm(form) {

    if(!form.checkValidity()) {
      return logger.error(
        'Form is not validated. Please fill in all required fields and try submit again.');
    }

    let customer = {
      accountNo: form['txtAccountNo'].value,
      customerName: form['txtCustomerName'].value,
      type: form['selAccountType'].value
    };

    let existCustomer = customers.filter(s => s.accountNo === customer.accountNo);
    if(existCustomer && existCustomer.length > 0) {
      alert('Customer ID is already taken.');
      return;
    }

    customers.push(customer);
    customerDbService.addCustomer(customer);
    ui_addCustomerToList(customer, true);
    form.reset();
  }

  /**
   * Display new registered customer.
   * @param customer
   * @param addAnimate
   */
  function ui_addCustomerToList(customer, addAnimate) {
    let lsCustomerElement = $('#lsCustomers');

    let customerElement =
      $(`<li class="show customer-item list-group-item">
          ${customer.accountNo} - ${customer.customerName}<i class="btn-delete">delete</i></li>`);
    $(lsCustomerElement).append(customerElement);

    if(typeof addAnimate === 'boolean' && addAnimate) {
      $(customerElement).addClass('blink-3');
    }

    let btnDelete = $('.btn-delete', customerElement);
    $(btnDelete).bind('click', function() {
      let confirmed = confirm(`Do you want to delete customer ${customer.customerName} ?`);
      if(!confirmed) return;

      deleteCustomer(customer, $(this).parent());
    });
  }

  function deleteCustomer(customer, liElement) {
    customerDbService.deleteCustomer(customer);
    let idx = customers.indexOf(customer);
    customers.splice(idx, 1);

    $(liElement).fadeOut(300, () => $(this).remove());
  }

})();

